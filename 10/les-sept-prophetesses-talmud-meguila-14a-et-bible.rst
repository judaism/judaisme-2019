.. index::
   pair: Prophètesses ; Houlda
   ! Les sept Prophètesses

.. _prophetesse_2019_10:

=======================================================================================================
2019-10  **Les sept Prophètesses/ Talmud Meguila 14a  et Bible**
=======================================================================================================


:download:`Télécharger le fichier au format docx <docx/meguila-14a-les-7-prophetesses-partiel.docx>`

Les sept Prophètesses/ Talmud Meguila 14a  et Bible
========================================================

Traditionnellement, on reçoit 7 invités sous la Souka : Abraham, Isaac,
Jacob, Moïse, Aharon, Joseph, David.

Le Talmud Méguila nous parle de 7 prophétesses, que nous pouvons inviter
également.

Choisissez l’une de ces personnalités et cherchez les qualités que vous
voulez retenir.

Attardez-vous sur Houlda. Quelle est l’importance de cette prophétesse ?

Qui sont les sept prophétesses ?
===================================

Sarah, Miriam, Deborah, Hannah, Abigail,
Huldah, et Esther, ainsi qu’il est écrit: “le père de malka et le père
de Iska” et Rabbi ItsHak a dit: Iska, c’est Sarah, et pourquoi a-t-elle
été appelée Iska?

Parce qu’elle contemplait le souffle sacré comme il est dit: “Tout ce que
te dira Sarah, écoute sa voix”. Autre chose: Yska car tous voyaient sa
beauté.

Myriam
------------

Myriam car il est écrit « Myriam la Prophétesse, sœur d’Aharon prit… »
Et pas la sœur de Moïse ?
Rabbi NaHman dit que Rav a dit quand elle était la sœur d’Aharon elle
disait « ma mère donnera naissance à un fils qui sauvera Israel » et
quand il est né la maison s’est emplie de lumière, son père s’est levé
et l’a embrassée sur le font et lui a dit : « ma fille, ta prophétie
s’est réalisée ».

Et quand il fut jeté dans le Nil son père s’est levé et lui a donné un
coup sur la tête et lui a dit « ma fille, ou est ta prophétie » c’est
pour cela qu’il est écrit : « et sa sœur se plaça de loin pour savoir
ce qu’il adviendrait de sa prophétie ».

Deborah
-------------

Deborah comme il est écrit : « et Déborah, une femme prophète, la femme
de Lapidot ». Que signifie « echet lapidot » ?
qu’elle faisait des mèches pour le sanctuaire. « et elle s’installait sous
un palmier ».

Qu’est-ce qu’il y a de spécial « sous un palmier » ? Rabbi Chimon fils
de Avchalom a dit : pour ne pas être soupçonnée de promiscuité.

Autre chose : de même que ce palmier n’a qu’un seul cœur, de même Israel
à cette époque n’avait qu’un seul cœur pour leur père qui est aux cieux.

Hanna
---------

Hanna comme il est écrit : « Et Hanna pria et dit mon cœur se réjouit en
XXXX , ma corne est exaltée », ma corne est exaltée et non pas ma cruche
est exaltée.

David et Salomon ont été oint par une corne et leur royauté a duré,
Saül et Jéhu ont été oint par une fiole et leur royauté n’a pas duré.

Abigaïl : Samuel I 25 :20s
================================

20 Or, tandis que, montée sur un âne, elle descendait par un pli de la
montagne, David et ses hommes descendaient à l'opposite; et elle les
rencontra. 21 David avait dit: "C'est donc en vain que j'ai préservé
tout ce que cet homme possédait dans le désert, de sorte qu'il n'a
éprouvé aucune perte!
Et lui m'a rendu le mal pour le bien! 22 Que Dieu en fasse autant et plus
aux ennemis de David, si d'ici au point du jour je laisse subsister, de
ce qui lui appartient, la moindre créature!" 23 

Abigaïl, en voyant David, descendit de l'âne en toute hâte, se jeta sur
la face devant David et se prosterna par terre. 24 Puis elle se jeta à
ses pieds et dit: "A moi, seigneur, à moi la faute!
Mais permets à ta servante de t'adresser quelques mots, et écoute les
paroles de ta servante. 25 
Que mon seigneur ne s'occupe pas de cet homme indigne, de Nabal, car il
ressemble à son nom: Nabal il se nomme, et nabal est son caractère.
Pour moi, ta servante, je n'ai pas vu les jeunes gens envoyés par mon
seigneur. 26 Et maintenant, seigneur, j'en atteste le Dieu vivant et ta
propre vie, ce Dieu qui t'aura préservé de t'engager dans le sang et de
te venger par ta propre main, oui, ils seront comme Nabal, tes ennemis,
ceux qui veulent du mal à mon seigneur. 27 Quant au présent que ta servante
a apporté à son seigneur, qu'il soit remis aux jeunes gens de sa suite.
28 Daigne faire grâce à ta servante! Certes, l'Eternel donnera à ta maison,
seigneur, une existence durable, car ce sont les guerres de l'Eternel
que tu soutiens, et, de ta vie, le malheur ne t'atteindra. 29 
Que si on s'avisait de t'attaquer et d'en vouloir à ta vie, l'existence
de mon seigneur restera liée au faisceau des vivants que protège
l'Eternel, ton Dieu, tandis qu'il lancera au loin, avec la fronde, celle
de tes ennemis. 30 Or, quand l'Eternel aura accompli pour toi, seigneur,
tout le bien qu'il t'a promis, et qu'il t'aura institué chef d'Israël, 31 
il ne faut pas que ceci devienne pour toi, seigneur, un écueil, une
cause de remords, d'avoir versé le sang inutilement, et de t'être fait
justice toi-même. L'Eternel te rendra heureux, seigneur, et tu te souviendras
de ta servante." 32 David répondit à Abigaïl: "Béni soit l'Eternel,
Dieu d'Israël, de t'avoir envoyée aujourd'hui à ma rencontre! 33 
Et bénie soit ta prudence et bénie sois-tu, toi qui m'as empêché
aujourd'hui de m'engager dans le sang et de demander secours à ma propre
main! 34 Mais certes, par l'Eternel, Dieu d'Israël, qui m'a préservé de
te faire du mal, si tu n'avais pas pris les devants en venant à moi,
certes, d'ici au point du jour pas la moindre créature ne fût restée à Nabal!"

35 David accepta de sa main ce qu'elle lui avait apporté, et lui dit:
"Retourne en paix chez toi; vois, j'ai cédé à ta parole et t'ai fait
bon accueil." 36 Abigaïl rejoignit Nabab qui faisait alors chez lui
un vrai festin de roi, était en belle humeur et s'était enivré à l'excès.

Elle ne lui adressa pas une parole jusqu'au lendemain matin. 37 Au matin,
l'ivresse de Nabal étant dissipée, sa femme lui conta ces faits; il sentit
son cœur défaillir et devint comme une pierre. 38 Environ dix jours après,
Nabal, frappé d'un coup du ciel, expirait. 39 En apprenant la mort de Nabal,
David dit: "Béni soit le Seigneur, qui s'est intéressé à mon injure et
en a puni Nabal! Il a épargné un méfait à son serviteur, et le méfait
de Nabal, il l'a fait retomber sur sa tête."
Puis David envoya faire parler à Abigaïl, désirant la prendre pour femme.
40 Les serviteurs de David vinrent trouver Abigaïl à Carmel, et lui dirent:
"David nous a envoyés vers toi, voulant te prendre pour épouse." 41 Et
elle se leva, se prosterna la face contre terre et dit: "Ta servante est
prête à laver, comme esclave, les pieds des serviteurs de mon maître."
42 Aussitôt Abigaïl se releva et monta sur un âne, suivie par ses cinq
servantes elle partit à la suite des envoyés de David et devint son épouse.
Houlda : II Rois 22 :8s
8 A cette occasion, Hilkiyyahou, le grand-prêtre, dit à Chafan, le secrétaire:
"Le livre de la loi a été trouvé par moi dans le temple du Seigneur et il
le remit à Chafan, qui le lut. 9 Chafan, le secrétaire, revint auprès du
roi et lui rendit compte de sa mission: "Tes serviteurs, dit-il, se sont
fait verser l'argent qui se trouvait dans le temple, et l'ont remis entre
les mains des directeurs des travaux, chargés de l'entretien de la maison
du Seigneur." 10 Il compléta ainsi son récit: "Hilkiyya, le pontife,
m'a remis un livre;" et Chafan en fit la lecture devant le roi.
11 En entendant les termes du livre de la loi, le roi déchira ses vêtements,
12 et donna cet ordre à Hilkiyya, le pontife, à Ahikam, fils de Chafan,
à Akhbor, fils de Mikhaya, à Chafan, le secrétaire, et à Assaya, officier
royal: 13 "Allez consulter l'Eternel pour moi, pour le peuple, pour tout
Juda, au sujet de ce livre qu'on vient de trouver, car grande est la colère
divine allumée contre nous par le refus de nos ancêtres à écouter et à
pratiquer les commandements qui nous sont prescrits dans ce livre."
14 Hilkiyyahou, le pontife, Ahikam, Akhbor, Chafan et Assaya se rendirent
auprès de la prophétesse Houlda, femme du gardien des vêtements,
Challoum, fils de Tikva, fils de Harhas; elle demeurait à Jérusalem dans
le deuxième quartier. Quand ils lui eurent parlé, 15 elle leur répliqua:
"Voici ce qu'a dit l'Eternel, Dieu d'Israël: Annoncez à l'homme qui vous
a envoyés auprès de moi 16 Ainsi a parlé l'Eternel: Je vais amener le
malheur sur cette contrée et ses habitants, toutes les choses prédites
dans le livre qu'a lu le roi de Juda, 17 parce qu'ils m'ont abandonné et
ont offert l'encens à des dieux étrangers, m'irritant par toutes les
œuvres de leurs mains; aussi ma colère s'est-elle allumée contre cette
contrée, pour ne plus s'éteindre. 18 Quant au roi de Juda qui vous envoie
pour consulter l'Eternel, voici ce que vous lui direz:
Ainsi a parlé l'Eternel, Dieu d'Israël, au sujet de ce que tu viens
d'entendre: 19 Puisque ton cœur s'est attendri, et que tu t'es humilié
devant l'Eternel en entendant que j'ai décrété la désolation et la
malédiction contre cette contrée et ses habitants, puisque tu as déchiré
tes vêtements et versé des larmes devant moi, de mon côté, je t'ai exaucé,
dit l'Eternel. 20 Je te réunirai donc à tes ancêtres, tu iras les
rejoindre en paix dans la tombe, et tes yeux ne verront pas les malheurs
que je déchaînerai sur cette contrée." Ils rendirent compte de leur
mission au roi. 1 Sur l'ordre du roi, l'on convoqua auprès
de lui tous les anciens de Juda et de Jérusalem. 2 Le roi monta au temple
du Seigneur,
accompagné de tous les Judéens et de tous les habitants de Jérusalem,
prêtres, prophètes et tout le peuple, petits et grands, et il leur donna
lecture de toutes les paroles du livre de l'alliance,
trouvé dans le temple du Seigneur. 3 Le roi se plaça sur l'estrade, et
s'engagea par un pacte, devant l'Eternel, à marcher dans ses voies, à
observer ses commandements, ses lois et ses statuts, de
tout cœur et de toute âme, afin d'accomplir les paroles de cette alliance,
inscrites dans ce livre. Tout le peuple adhéra au pacte.



